console.log('vue', Vue)
console.log('echarts', echarts)

const animationDuration = 1000

// 防抖函数
let debounce = (fn, delay) => {
    return echarts.throttle(fn, delay, true)
}

let CommonSectionHeader = {
    template: `
         <div class="section-item-header">
            <span class="header-inner-line"></span>
            <span class="header-inner-text">{{text}}</span>
            <span class="header-inner-line"></span>
         </div>
    `,
    props: {
        text: String
    }
}

let ChartMixin = {
    data() {
        return {
            chart: null,
            resizeHandler: null
        }
    },
    mounted() {
        this.initResizeHandler()
        this.$nextTick(() => {
            this.initChart()
        })
    },
    beforeDestroy() {
        this.destroyListener()
        if (!this.chart) {
            return
        }
        this.chart.dispose()
        this.chart = null
    },
    methods: {
        resize() {
            this.chart && this.chart.resize()
        },
        initResizeHandler() {
            this.resizeHandler = debounce(() => {
                this.resize()
            }, 100)
            window.addEventListener('resize', this.resizeHandler)
        },
        destroyListener() {
            window.removeEventListener('resize', this.resizeHandler)
        }
    }
}


// 近5年研发投入
let RDLast5 = {
    template: `
        <div id="rd-last5"></div>
    `,
    mixins: [ChartMixin],
    methods: {
        initChart() {
            this.chart = echarts.init(this.$el)

            this.chart.setOption({
                color: ['rgb(253, 108, 108)', 'rgb(53, 196, 196)'],
                textStyle: {
                    color: '#fff'
                },
                legend: {
                    data: ['研发经费', 'GDP占比'],
                    textStyle: {
                        color: '#fff'
                    },
                    icon: 'rect',
                    itemWidth: 10,
                    itemHeight: 10,
                },
                tooltip: {
                    show: true
                },
                grid: {
                    left: '4%',
                    right: '8%',
                    top: '20%',
                    bottom: '4%',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    name: '年份',
                    axisLine: {
                        // symbol: ['none', 'arrow'],
                        // symbolSize: [6, 6],
                        lineStyle: {
                            color: '#fff',
                        }
                    },
                    axisTick: {
                        show: false
                    },
                    // data: []
                    data: ['2017', '2018', '2019', '2020', '2021']
                },
                yAxis: [
                    {
                        type: 'value',
                        name: '研发经费（亿）',
                        axisLine: {
                            show: true,
                            // symbol: ['none', 'arrow'],
                            // symbolSize: [6, 6],
                            lineStyle: {
                                color: '#fff'
                            }
                        },
                        splitLine: {
                            show: false,
                        }
                    },
                    {
                        type: 'value',
                        show: false,
                        name: 'GDP占比',
                        max: 100,
                        // alignTicks: true,
                    },
                ],
                series: [
                    {
                        name: '研发经费',
                        type: 'bar',
                        yAxisIndex: 0,
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
                                formatter: '{c}',
                                fontSize: 9

                            }
                        },
                        // data: [],
                        data: [2096, 2308, 1833, 2096, 2308],
                    },
                    {
                        name: 'GDP占比',
                        type: 'line',
                        yAxisIndex: 1,
                        // data: [],
                        max: 100,
                        data: [29, 33, 22, 29, 33],
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
                                formatter: '{c}%',
                                fontSize: 9
                            }
                        },

                    }
                ]
            })
        }

    }
}

// 科技项目立项情况
let ProjectApprovalLast5 = {
    template: `
        <div id="project-approval-last5"></div> 
    `,
    mixins: [ChartMixin],
    methods: {
        initChart() {
            this.chart = echarts.init(this.$el)

            this.chart.setOption({
                color: ['rgb(253, 108, 108)', 'rgb(53, 196, 196)'],
                textStyle: {
                    color: '#fff'
                },
                legend: {
                    data: ['立项数', '立项金额'],
                    textStyle: {
                        color: '#fff'
                    },
                    icon: 'rect',
                    itemWidth: 10,
                    itemHeight: 10,
                },
                tooltip: {
                    show: true
                },
                grid: {
                    left: '4%',
                    right: '8%',
                    top: '20%',
                    bottom: '4%',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    name: '年份',
                    axisLine: {
                        // symbol: ['none', 'arrow'],
                        // symbolSize: [6, 6],
                        lineStyle: {
                            color: '#fff',
                        }
                    },
                    axisTick: {
                        show: false
                    },
                    // data: []
                    data: ['2017', '2018', '2019', '2020', '2021']
                },
                yAxis: [
                    {
                        type: 'value',
                        name: '',
                        axisLine: {
                            show: true,
                            // symbol: ['none', 'arrow'],
                            // symbolSize: [6, 6],
                            lineStyle: {
                                color: '#fff'
                            }
                        },
                        splitLine: {
                            show: false,
                        }
                    },
                    {
                        type: 'value',
                        show: false,
                        name: 'GDP占比',
                        max: 100,
                        // alignTicks: true,
                    },
                ],
                series: [
                    {
                        name: '立项数',
                        type: 'bar',
                        yAxisIndex: 0,
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
                                formatter: '{c}',
                                fontSize: 9

                            }
                        },
                        // data: [],
                        data: [2096, 2308, 1833, 2096, 2308],
                    },
                    {
                        name: '立项金额',
                        type: 'bar',
                        yAxisIndex: 1,
                        // data: [],
                        max: 100,
                        data: [29, 33, 22, 29, 33],
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
                                formatter: '{c}',
                                fontSize: 9
                            }
                        },

                    }
                ]
            })
        }

    }
}

// 科技热点
// url: /techhotspots/list
let TechHotspot = {
    template: `
        <div id="tech-hotspot"></div>
    `,
    mixins: [ChartMixin],
    data() {
        return {}
    },
    methods: {
        initChart() {
            this.chart = echarts.init(this.$el)

            this.chart.setOption({
                textStyle: {
                    color: '#fff'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: { // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                grid: {
                    top: '20',
                    left: '2%',
                    right: '2%',
                    bottom: '10',
                    containLabel: true
                },
                xAxis: [{
                    type: 'category',
                    data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                    axisTick: {
                        alignWithLabel: true
                    }
                }],
                yAxis: [{
                    type: 'value',
                    axisTick: {
                        show: false
                    }
                }],
                series: [{
                    name: 'pageA',
                    type: 'bar',
                    stack: 'vistors',
                    barWidth: '60%',
                    data: [79, 52, 200, 334, 390, 330, 220],
                    animationDuration
                }, {
                    name: 'pageB',
                    type: 'bar',
                    stack: 'vistors',
                    barWidth: '60%',
                    data: [80, 52, 200, 334, 390, 330, 220],
                    animationDuration
                }, {
                    name: 'pageC',
                    type: 'bar',
                    stack: 'vistors',
                    barWidth: '60%',
                    data: [30, 52, 200, 334, 390, 330, 220],
                    animationDuration
                }]
            })
        }
    }
}

// 全省排名
let ProvincialRanking = {
    template: `
        <div id="provincial-ranking">
        </div>
    `,
    mixins: [ChartMixin],
    methods: {
        initChart() {
            this.chart = echarts.init(this.$el)

            this.chart.setOption({
                color: ['rgb(46, 199, 202)', 'rgb(178, 161, 225)'],
                textStyle: {
                    color: '#fff'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: { // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                grid: {
                    top: 8,
                    left: 8,
                    right: 24,
                    bottom: 8,
                    containLabel: true
                },
                xAxis: [{
                    type: 'value',
                    show: false,
                    axisLine: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                }],
                yAxis: [{
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    inverse: true,
                    data: ['研发经费', '高新技术企业', '科技型中小企业', '省级创新平台', '孵化载体', '技术合同成交额'],
                }],
                series: [
                    {
                        name: '全省排名',
                        type: 'pictorialBar',
                        colorBy: 'data',
                        data: [7, 8, 8, 7, 7, 8],
                        symbol: 'rect',
                        symbolSize: [4, 8],
                        symbolRepeat: true,
                        symbolClip: false,
                        animationDuration
                    },
                    // label
                    {
                        name: '全省排名',
                        type: 'pictorialBar',
                        label: {
                            show: true,
                            position: 'right',
                        },
                        data: [7, 8, 8, 7, 7, 8],
                        symbol: 'rect',
                        symbolSize: [4, 8],
                        symbolRepeat: 'fixed',
                        itemStyle: {
                            color: 'transparent',
                        },
                        animationDuration
                    },
                    // 背景
                    {
                        name: '全省排名',
                        type: 'pictorialBar',
                        // label: {
                        //     show: true,
                        //     position: 'right',
                        // },
                        data: [7, 8, 8, 7, 7, 8],
                        symbol: 'rect',
                        symbolSize: [4, 8],
                        symbolRepeat: 'fixed',
                        itemStyle: {
                            opacity: 0.2,
                        },
                        animationDuration
                    }]
            })
        }
    }
}

// 概览
// url: /kingconfig/kingconfig-list-for-index data: {"configType":["1","2"],"sysType":"hy"}
let CityOverview = {
    template: `
        <div class="city-overview">
            <div class="overview-item" v-for="(item, index) in list">
                <p class="item-label">{{item.label}}</>
                <p class="item-wrapper">
                    <span class="item-value">{{item.value}}</span>   
                    <span class="item-unit" v-if="item.unit">{{item.unit}}</span>
                </p>
            </div>
        </div>
    `,
    props: {
        list: Array,
    },
}

// 地图
let CityMap = {
    template: `
        <div id="city-map">
        </div>
    `,
    mixins: [ChartMixin],
    methods: {
        initChart() {
            echarts.registerMap('惠州', MapData)
            this.chart = echarts.init(this.$el)

            this.chart.setOption({
                // backgroundColor: '#404a59',
                itemStyle: {
                    color: '#fff'
                },
                series: [
                    {
                        name: '惠州市数据',
                        type: 'map',
                        map: '惠州',
                        showLegendSymbol: true,
                        zoom: 1.2,
                        itemStyle: {
                            normal: {
                                borderWidth: 1, // 边界线宽度
                                areaColor: '#0CA8EB',
                                label: {
                                    show: true,
                                    color: '#fff'
                                },
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    color: '#fff'
                                },
                            }
                        },
                    }
                ]
            })
        }

    }
}

// 全市R&D、GDP历年情况
let CityGdp = {
    template: `
        <div id="city-gdp-rd"></div>
    `,
    data() {
        return {
            chart: null
        }
    },
    mounted() {
        this.$nextTick(() => {
            this.initChart()
        })
    },
    beforeDestroy() {
        if (!this.chart) {
            return
        }
        this.chart.dispose()
        this.chart = null
    },
    methods: {
        initChart() {
            this.chart = echarts.init(this.$el)

            this.chart.setOption({
                textStyle: {
                    color: '#fff'
                },
                // title: {
                //     text: '全市R&D/GDP历年情况',
                //     x: 'center',
                //     y: '3%',
                //     textStyle: {
                //         fontSize: 16,
                //         fontWeight: 'bold',
                //         color: '#fff'
                //     }
                // },
                legend: {
                    data: ['R&D/GDP', 'GDP', 'R&D'],
                    y: '0',
                    textStyle: {
                        color: '#fff'
                    }
                },
                tooltip: {
                    trigger: 'axis',
                    formatter: function (params) {
                        // console.log(params)
                        var name = params[0].name + '<br>'
                        if (params[0])
                            name += params[0].seriesName + '：' + params[0].value + '%<br>';
                        if (params[1])
                            name += params[1].seriesName + '：' + params[1].value + '亿元<br>';
                        if (params[2])
                            name += params[2].seriesName + '：' + params[2].value + '亿元<br>';
                        return name
                    }
                },
                grid: {
                    left: '2%',
                    right: '0',
                    top: '16%',
                    bottom: '2%',
                    containLabel: true
                },
                color: ['#2ec9c8', '#b3c0c9', '#c99378'],
                xAxis: {
                    type: 'category',
                    boundaryGap: true,
                    // data: []
                    data: ['2010年', '2011年', '2012年', '2013年', '2014年', '2015年']
                },
                yAxis: [
                    {
                        type: 'value',
                        name: 'R&D/GDP',
                        /*max:function(value){
                            return value.max+0.01;
                            },*/
                        axisLabel: {
                            formatter: '{value} %'
                        },
                        splitArea: {
                            show: false,
                            // areaStyle: {
                            //     color: ['#fafafa', '#ffffff']
                            // }
                        },
                        splitLine: {
                            show: true,
                            lineStyle: {
                                color: ['#f0f0f0']
                            }
                        }


                    },
                    {
                        type: 'value',
                        // max: 20000,
                        name: 'GDP',
                        position: 'right',
                        axisLabel: {
                            formatter: '{value} ',
                            //margin : 20

                        },
                        nameGap: 30,
                        //nameRotate: 40,
                        // splitArea: {
                        //     show: true,
                        //     areaStyle: {
                        //         color: ['#fafafa', '#ffffff']
                        //     }
                        // },
                        // splitLine: {
                        //     show: true,
                        //     lineStyle: {
                        //         color: ['#f0f0f0']
                        //     }
                        // }


                    },
                    {
                        type: 'value',
                        name: 'R&D',
                        // max: 30,
                        position: 'right',
                        axisLabel: {
                            formatter: '{value} 亿元'
                        },
                        splitArea: {
                            show: true,
                            areaStyle: {
                                color: ['#fafafa', '#ffffff']
                            }
                        },
                        splitLine: {
                            show: true,
                            lineStyle: {
                                color: ['#f0f0f0']
                            }
                        }
                    },


                ],
                series: [
                    {
                        name: 'R&D/GDP',
                        type: 'line',
                        smooth: 'true',
                        stack: '总量',
                        // data: [],
                        data: [0.09, 0.15, 0.21, 0.3, 0.3, 0.3],
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
                                formatter: '{c}%'
                            }
                        },

                    },
                    {
                        name: 'GDP',
                        type: 'bar',
                        yAxisIndex: 1,
                        itemStyle: {},
                        // barWidth: 30,
                        //barGap: '30%', // Make series be overlap
                        //stack: '总量',
                        // data: [],
                        data: [0.09, 0.15, 0.21, 0.3, 0.3, 0.3],
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
                                formatter: '{c}',
                                fontSize: 9

                            }
                        },

                    }
                    , {
                        name: 'R&D',
                        type: 'bar',
                        yAxisIndex: 1,
                        // barWidth: 30,
                        // stack: '总量',
                        // data: [],
                        data: [0.09, 0.15, 0.21, 0.3, 0.3, 0.3],
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
                                formatter: '{c}',
                                fontSize: 9
                            }
                        },

                    }
                ]
            })
        }
    }
}

// 近三年高企增长情况
let HighGrowthLast3 = {
    template: `
        <div id="high-growth-last3"></div>
    `,
    data() {
        return {
            chart: null
        }
    },
    mounted() {
        this.$nextTick(() => {
            this.initChart()
        })
    },
    beforeDestroy() {
        if (!this.chart) {
            return
        }
        this.chart.dispose()
        this.chart = null
    },
    methods: {
        initChart() {
            this.chart = echarts.init(this.$el)
            this.chart.setOption({
                textStyle: {
                    color: '#fff'
                },
                // title : {
                //     text : '高企近3年增长情况',
                //     x : 'center',
                //     // y : '3%',
                //     textStyle : {
                //         fontSize : 16,
                //         fontWeight : 'bold'
                //     }
                // },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    top: '4',
                    data: ['高企存量']
                },
                grid: {
                    left: 10,
                    top: '10%',
                    bottom: 10,
                    right: 10,
                    containLabel: true
                },
                xAxis: [
                    {
                        name: '年份',
                        type: 'category',
                        data: ["2016", "2017", "2018"]
                    },
                ],
                yAxis: [
                    {
                        name: '',
                        type: 'value',
                        // splitArea: {
                        //     show: true,
                        //     areaStyle: {
                        //         color: ['#fafafa', '#ffffff']
                        //     }
                        // },
                        splitLine: {
                            show: true,
                            lineStyle: {
                                color: ['#f0f0f0']
                            }
                        }
                    }
                ],
                series: [
                    {
                        name: '高企存量',
                        type: 'line',
                        data: [80, 125, 166],
                        label: {
                            normal: {
                                show: true,
                                position: 'top'
                            }
                        },

                    }
                ]
            })
        }
    }
}

// 高企存量
let HighStock = {
    template: `
        <div id="high-stock"></div> 
    `,
    mixins: [ChartMixin],
    methods: {
        initChart() {
            this.chart = echarts.init(this.$el)

            this.chart.setOption({
                color: ['rgb(46, 199, 202)', 'rgb(178, 161, 225)'],
                textStyle: {
                    color: '#fff'
                },
                legend: {
                    data: ['高企存量', '科技型中小企业'],
                    textStyle: {
                        color: '#fff'
                    },
                    icon: 'rect',
                    itemWidth: 10,
                    itemHeight: 10,
                },
                tooltip: {
                    show: true
                },
                grid: {
                    left: '4%',
                    right: '8%',
                    top: '20%',
                    bottom: '4%',
                    containLabel: true
                },
                xAxis: {
                    type: 'category',
                    name: '',
                    axisLine: {
                        // symbol: ['none', 'arrow'],
                        // symbolSize: [6, 6],
                        lineStyle: {
                            color: '#fff',
                        }
                    },
                    axisTick: {
                        show: false
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: 'rgba(55, 85, 105, 0.5)'
                        }
                    },
                    // data: []
                    data: ['广州', '深圳', '珠海', '佛山', '东莞', '惠州', '中山', '江门', '肇庆']
                },
                yAxis: [
                    {
                        type: 'value',
                        name: '',
                        axisLine: {
                            show: true,
                            // symbol: ['none', 'arrow'],
                            // symbolSize: [6, 6],
                            lineStyle: {
                                color: '#fff'
                            }
                        },
                        splitLine: {
                            show: true,
                            lineStyle: {
                                color: 'rgba(55, 85, 105, 0.5)'
                            }
                        },
                    },
                ],
                series: [
                    {
                        name: '高企存量',
                        type: 'bar',
                        // data: [],
                        data: [209, 230, 183, 209, 238, 206, 238, 183, 200],
                    },
                    {
                        name: '科技型中小企业',
                        type: 'bar',
                        // data: [],
                        data: [29, 33, 22, 29, 33, 29, 33, 22, 29,],

                    }
                ]
            })
        }

    }
}

// 创新平台

// 图表
let InnovationPlatformChart = {
    template: `
        <div id="innovation-platform-chart"></div>
    `,
    mixins: [ChartMixin],
    methods: {
        initChart() {
            this.chart = echarts.init(this.$el)

            this.chart.setOption({
                textStyle: {
                    color: '#fff'
                },
                color: ['#2ABCCB', '#FF6C6A'],
                tooltip: {
                    trigger: 'item'
                },
                series: [
                    {
                        name: '',
                        type: 'pie',
                        radius: ['40%', '70%'],
                        avoidLabelOverlap: false,
                        label: {
                            show: true,
                            // position: 'center'
                        },
                        labelLine: {
                            show: true,
                        },
                        // emphasis: {
                        //     label: {
                        //         show: true,
                        //         fontSize: '40',
                        //         fontWeight: 'bold'
                        //     }
                        // },
                        data: [
                            { value: 3, name: '市级' },
                            { value: 5, name: '省级' },
                        ]
                    }
                ]
            })
        }
    }
}

let InnovationPlatform = {
    template: `
        <div class="innovation-platform">
            <div class="platform-tabs">
                <template v-for="item in tabs">
                   <div class="platform-tab-item" :class="{ 'active': item.key === active}" :key="item.key" @click="handleTabClick(item)">{{item.name}}</div>
                </template>
            </div>
            <div class="platform-chart">
                <innovation-platform-chart class="chart-el" />
            </div>
        </div>
    `,
    components: {
        'innovation-platform-chart': InnovationPlatformChart
    },
    data() {
        return {
            active: '1',
            tabs: [
                {
                    name: '实验室',
                    key: '1'
                },
                {
                    name: '政府共建研究中心',
                    key: '2'
                },
                {
                    name: '新型研发机构',
                    key: '3'
                },
                {
                    name: '重点实验室',
                    key: '4'
                },
                {
                    name: '工程技术研究中心',
                    key: '5'
                }
            ]
        }
    },
    methods: {
        handleTabClick({key}) {
            if (this.active === key) return
            this.active = key
        }
    }
}



// 技术合同登记
let TechContractRegistration = {
    template: `
        <div id="tech-contract-registration" class="tech-contract-registration">
            <p class="registration-time">
               <span>统计时间</span>
               <span>2022.10.10~2022.11.30</span>
            </p>
            <div class="registration-item quantity">
                <p class="item-label">技术合同成交量</p>
                <span class="item-value">23,303</span>
            </div>
            <div class="registration-item amount">
                <p class="item-label">技术合同成交额</p>
                <p>
                    <span class="item-prefix">¥</span>
                    <span class="item-value">162,078,323,184,84</span>
                </p>
            </div>
            <div class="registration-item trade">
                <p class="item-label">技术合同技术交易额</p>
                <p>
                    <span class="item-prefix">¥</span>
                    <span class="item-value">162,078,323,184,84</span>
                </p>
            </div>
        </div> 
    `
}

// 科技政策
let TechPolicy = {
    template: `
        <ul id="tech-policy" class="tech-policy-list">
            <template v-for="(item, index) in list">
                <li class="tech-policy-item" :key="index">
                    <span class="item-text">{{item.text}}</span>
                    <span class="item-time">{{item.time}}</span>
                </li>
            </template>

        </ul> 
    `,
    props: {
        list: Array
    },
}

// 高企申报进度
let HighReportProgress = {
    template: `
        <div class="report-overview-list">
            <div class="report-overview-wrapper" v-for="(item, index) in list" :key="index">
                <p class="report-label">第{{numDict[index]}}批：</p>
                <div class="report-overview">
                    <div class="overview-item">
                        <p class="item-label">申报</>
                        <p class="item-wrapper">
                            <span class="item-value">{{item.report}}</span>   
                            <span class="item-unit">家</span>
                        </p>
                    </div>
                    <div class="overview-item">
                        <p class="item-label">推荐</>
                        <p class="item-wrapper">
                            <span class="item-value">{{item.recommend}}</span>   
                            <span class="item-unit">家</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    `,
    props: {
        list: Array,
    },
    data() {
        return {
            numDict: ['一', '二', '三']
        }
    },
}

let IndexPageVue = new Vue({
    el: '#index-page',
    components: {
        'common-section-header': CommonSectionHeader,
        'rd-last5': RDLast5,
        'tech-hotspot': TechHotspot,
        'city-overview': CityOverview,
        'city-gdp': CityGdp,
        'city-map': CityMap,
        'provincial-ranking': ProvincialRanking,
        'high-growth-last3': HighGrowthLast3,
        'project-approval-last5': ProjectApprovalLast5,
        'contract-registration': TechContractRegistration,
        'tech-policy': TechPolicy,
        'high-stock': HighStock,
        'high-report-progress': HighReportProgress,
        'innovation-platform': InnovationPlatform
    },
    mixins: [DrawMixin],
    data() {
        return {
            title: '惠州市科技创新监测平台',
            tabs: [
                {
                    name: '研发投入',
                },
                {
                    name: '科技项目',
                },
                {
                    name: '高企培育',
                },
                {
                    name: '创新平台',
                },
                {
                    name: '成果转化',
                },
                {
                    name: '科技画像',
                },
                {
                    name: '科技政策',
                },
                {
                    name: '管理中心',
                }
            ],
            sections: [],
            overviewList: [
                {
                    label: '研发人员',
                    key: '',
                    value: '47397',
                    unit: '人年'
                },
                {
                    label: '研发投入强度',
                    key: '',
                    value: '3.0%',
                    unit: ''
                },
                {
                    label: '省重点领域研发立项数',
                    key: '',
                    value: '1481',
                    unit: '项'
                },
                {
                    label: '高企存量',
                    key: '',
                    value: '2091',
                    unit: '家'
                },
                {
                    label: '省级新型研发机构',
                    key: '',
                    value: '13',
                    unit: '家'
                },
                {
                    label: '省级重点实验室',
                    key: '',
                    value: '396',
                    unit: '家'
                },
                {
                    label: '省级工程技术研究中心',
                    key: '',
                    value: '5944',
                    unit: '家'
                },
                {
                    label: '国家级孵化器',
                    key: '',
                    value: '8',
                    unit: '家'
                },
                {
                    label: '省级孵化器',
                    key: '',
                    value: '7',
                    unit: '家'
                },
                {
                    label: '国家级众创空间',
                    key: '',
                    value: '10',
                    unit: '家'
                },
                {
                    label: '省级众创空间',
                    key: '',
                    value: '3',
                    unit: '家'
                },
                {
                    label: '加速器',
                    key: '',
                    value: '10',
                    unit: '家'
                }
            ],
            // 申报进度
            reportProgressList: [
                {
                    report: 302,
                    recommend: 302
                },
                {
                    report: 448,
                    recommend: 441
                },
                {
                    report: 718,
                    recommend: 623
                }
            ],
            techPolicyList: [
                {
                    text: '科技政策科技政策科技政策科技政策科技政策',
                    time: '2022/12/07'
                },
                {
                    text: '科技政策科技政策科技政策科技政策科技政策',
                    time: '2022/12/07'
                },
                {
                    text: '科技政策科技政策科技政策科技政策科技政策',
                    time: '2022/12/07'
                },
                {
                    text: '科技政策科技政策科技政策科技政策科技政策',
                    time: '2022/12/07'
                },
                {
                    text: '科技政策科技政策科技政策科技政策科技政策',
                    time: '2022/12/07'
                },
                {
                    text: '科技政策科技政策科技政策科技政策科技政策',
                    time: '2022/12/07'
                },
                {
                    text: '科技政策科技政策科技政策科技政策科技政策',
                    time: '2022/12/07'
                },
                {
                    text: '科技政策科技政策科技政策科技政策科技政策',
                    time: '2022/12/07'
                }
            ],
        }
    }
})
